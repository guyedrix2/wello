from flask import Flask, jsonify, render_template, request
import subprocess
from kubernetes import client, config
import json  

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/check_deployment')
def check_deployment():
    try:
        # Run kubectl get deployments command
        result = subprocess.run(['kubectl', 'get', 'deployments', '-A'], capture_output=True, text=True)
        output = result.stdout

        # Extract deployment data
        deployment_data = []
        lines = output.split('\n')
        for line in lines[1:]:
            if line:
                parts = line.split()
                deployment_data.append({
                    'name': parts[0],
                    'namespace': parts[1],
                    'replicas': parts[2],
                    'available': parts[3],
                    'unavailable': parts[4]
                })

        # Render the HTML template with deployment data
        return render_template('checkdeployment.html', deployment_data=deployment_data)
    except Exception as e:
        return render_template('error.html', message=str(e))

@app.route('/describe_deployment/<deployment_name>')
def describe_deployment(deployment_name):
    try:
        # Run kubectl describe deployment command
        result = subprocess.run(['kubectl', 'describe', 'deployment', deployment_name], capture_output=True, text=True)
        output = result.stdout
        
        # Split the output into lines and filter out empty lines
        lines = [line.strip() for line in output.split('\n') if line.strip()]

        # Pass parsed deployment details to the template
        return render_template('describe_deployment.html', deployment_details=lines)
    except Exception as e:
        return render_template('error.html', message=str(e))

@app.route('/namespace', methods=['GET'])
def get_pod_status():
    # Get namespace from query parameters
    namespace = request.args.get('namespace')

    # Load Kubernetes config
    config.load_kube_config()

    # Create Kubernetes API client
    v1 = client.CoreV1Api()

    # Fetch pod information
    pods = v1.list_namespaced_pod(namespace)

    # Extract and display pod information
    pod_status = []
    for pod in pods.items:
        pod_status.append({
            "name": pod.metadata.name,
            "image": pod.spec.containers[0].image,
            "status": pod.status.phase
        })

    return jsonify(pod_status)

@app.route('/delete', methods=['POST'])
def delete_all_resources_and_namespace():
    try:
        namespace = request.form.get('namespace')
        if not namespace:
            raise ValueError("Namespace is required")

        # Delete all resources in the namespace
        subprocess.run(['kubectl', 'delete', 'all', '--all', '--namespace', namespace], check=True)
        
        # Delete other resources specific to the namespace (like configmaps, secrets, etc.)
        subprocess.run(['kubectl', 'delete', 'configmap', '--all', '--namespace', namespace], check=True)
        subprocess.run(['kubectl', 'delete', 'secret', '--all', '--namespace', namespace], check=True)
        # Add other resource types to delete as needed
        
        # Delete the namespace
        subprocess.run(['kubectl', 'delete', 'namespace', namespace], check=True)
        
        message = f"All resources in namespace '{namespace}' and the namespace itself deleted successfully."
    except subprocess.CalledProcessError as e:
        message = f"Error deleting resources and namespace '{namespace}': {e}"
    except Exception as e:
        message = f"Error: {e}"

    return jsonify({'message': message})


@app.route('/deploy', methods=['POST'])
def deploy_app():
    try:
        # Parse request data
        data = request.get_json()
        namespace = data.get('namespace')
        if not namespace:
            raise ValueError("Namespace is required")
        
        # Ensure that the Helm chart path is provided
        helm_chart = data.get('helm_chart')
        if not helm_chart:
            raise ValueError("Helm chart path is required")

        # Check if the namespace exists, and create it if it doesn't
        subprocess.run(['kubectl', 'get', 'namespace', namespace], check=False)
        subprocess.run(['kubectl', 'create', 'namespace', namespace], check=True)

        # Deploy the application using Helm to the specified namespace
        subprocess.run(['helm', 'upgrade', '--install', '--namespace', namespace, 'my-release', helm_chart], check=True)

        message = f"Application deployed successfully to namespace '{namespace}' using Helm."
        return jsonify({'message': message}), 200
    except subprocess.CalledProcessError as e:
        message = f"Error deploying application to namespace '{namespace}' using Helm: {str(e)}"
        return jsonify({'message': message}), 500
    except ValueError as e:
        message = f"Error: {str(e)}"
        return jsonify({'message': message}), 400
    except Exception as e:
        message = f"Internal Server Error: {str(e)}"
        return jsonify({'message': message}), 500
        
if __name__ == '__main__':
    app.run(host='0.0.0.0')
