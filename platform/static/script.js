// Function to fetch deployment data from Flask route '/check_deployment'
function fetchDeployments() {
    $.get('/check_deployment', function(data) {
        $('#deploymentsTable tbody').empty();
        data.output.split('\n').forEach(function(row) {
            var columns = row.split(/\s+/);
            if (columns.length >= 5 && columns[0] != 'NAME') {
                $('#deploymentsTable tbody').append('<tr><td>' + columns[0] + '</td><td>' + columns[1] + '</td><td>' + columns[2] + '</td><td>' + columns[3] + '</td><td>' + columns[4] + '</td></tr>');
            }
        });
    });
}

// Call fetchDeployments function on page load
$(document).ready(function() {
    fetchDeployments();
});
